package com.rubrica;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

public class Screen_rubrica extends JFrame {
    private JPanel rootPanel;
    private JTable table_contatti;
    private JButton aggiungiButton;
    private JButton cancellaButton;
    private JButton modificaButton;
    private JPanel TitoloPanel;
    private JPanel BottoniPanel;
    private JPanel TabellaPanel;
    private JLabel title_label;
    public static ArrayList<Persona> persone;
    public static DefaultTableModel model;

    public Screen_rubrica(){
        super("Contatti");
        this.setContentPane(this.rootPanel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.title_label.setFont(new Font("Segoe UI",Font.BOLD,24));
        persone = new ArrayList<Persona>();
        model = new DefaultTableModel();
        aggiungiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Screen_add add = new Screen_add(-1);
                add.setVisible(true);
            }
        });
        cancellaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int i = table_contatti.getSelectedRow();
                if(i >=0){
                    int val = JOptionPane.showConfirmDialog(null,"Vuoi eliminare il contatto: " +persone.get(i).getNome() + " " + persone.get(i).getCognome() + "?","Messaggio di Conferma",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if( val == 0){
                        model.removeRow(i);
                        persone.remove(i);
                        refresh_file();
                    }
                }
                else{
                    JOptionPane.showMessageDialog(null,"Devi selezionare un contatto per cancellarlo","Messaggio di Errore",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        modificaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int i = table_contatti.getSelectedRow();
                if(i >=0){
                    Screen_add add = new Screen_add(i);
                    add.setVisible(true);
                }
                else{
                    JOptionPane.showMessageDialog(null,"Devi selezionare un contatto per modificarlo","Messaggio di Errore",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    public static void refresh_file(){
        String root = System.getProperty("user.dir");
        File file = new File(root + "/informazioni.txt");
        boolean delete = file.delete();
        if(delete){
            File new_file = new File(root + "/informazioni.txt");
            FileWriter writer = null;
            try {
                writer = new FileWriter(new_file);
                for (Persona persona : persone) {
                    String line = persona.getNome() + ";" + persona.getCognome() + ";" + persona.getIndirizzo() + ";" + persona.getNum_telefono() + ";" + String.valueOf(persona.getEta()) + "\n";
                    writer.write(line);
                }
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private ArrayList<Persona> read_file(File file){
        BufferedReader reader;
        ArrayList<Persona> contatti= new ArrayList<>();
        try{
            reader =new BufferedReader(new FileReader (file));
            String line = reader.readLine();
            while (line != null){
                String delims = "[;]";
                String[] dati_contatto = line.split(delims);
                Persona p = new Persona(dati_contatto[0],dati_contatto[1],dati_contatto[3],dati_contatto[2],Integer.parseInt(dati_contatto[4]));
                contatti.add(p);
                line = reader.readLine();
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return contatti;
    }

    private void create_Table(ArrayList<Persona> contatti){
        Object [] colonne = {"Nome" , "Cognome" , "N.Telefono"};
        model.setColumnIdentifiers(colonne);
        table_contatti.getTableHeader().setFont(new Font("Segoe UI",Font.BOLD,14));
        table_contatti.getTableHeader().setOpaque(false);
        table_contatti.getTableHeader().setBackground(new Color(32,136,203));
        table_contatti.getTableHeader().setForeground(new Color(255,255,255));
        table_contatti.setRowHeight(25);
        table_contatti.setModel(model);
        table_contatti.setShowHorizontalLines(true);
        table_contatti.setGridColor(Color.black);
        if(!contatti.isEmpty()){
            for (Persona persona : contatti) {
                Object[] row = new Object[3];
                row[0] = persona.getNome();
                row[1] = persona.getCognome();
                row[2] = persona.getNum_telefono();
                model.addRow(row);
            }
        }
    }

    public static void main(String[] args){
        Screen_rubrica rubrica = new Screen_rubrica();
        rubrica.setVisible(true);
        String root = System.getProperty("user.dir");
        String path = root + "/informazioni.txt";
        java.net.URL logoOneUrl = Screen_rubrica.class.getResource("/icon/rubrica1.png");
        rubrica.title_label.setIcon(new ImageIcon(logoOneUrl));
        try {
            File file = new File(path);
            if (file.exists()){
                System.out.println("Il file " + path + " esiste");
                persone = rubrica.read_file(file);
            }
            else if (file.createNewFile())
                System.out.println("Il file " + path + " è stato creato");
            else
                System.out.println("Il file " + path + " non può essere creato");
        } catch (IOException e) {
            e.printStackTrace();
        }

        rubrica.create_Table(persone);
    }
}

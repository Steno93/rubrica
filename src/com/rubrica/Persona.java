package com.rubrica;

public class Persona {

    private String nome;
    private String cognome;
    private String num_telefono;
    private String indirizzo;
    private Integer eta;

    public Persona(String nome, String cognome, String num_telefono, String indirizzo, Integer eta) {
        this.nome = nome;
        this.cognome = cognome;
        this.num_telefono = num_telefono;
        this.indirizzo = indirizzo;
        this.eta = eta;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getNum_telefono() {
        return num_telefono;
    }

    public void setNum_telefono(String num_telefono) {
        this.num_telefono = num_telefono;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public Integer getEta() {
        return eta;
    }

    public void print_persona(Persona p){
        System.out.println("Nome: " + p.getNum_telefono());
    }

}

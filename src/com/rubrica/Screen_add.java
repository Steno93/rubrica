package com.rubrica;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Screen_add extends JFrame {
    private JPanel root_addPanel;
    private JPanel title_addPanel;
    private JPanel bodyPanel;
    private JPanel button_editorPanel;
    private JButton button_annulla;
    private JTextField textField_nome;
    private JTextField textField_cognome;
    private JTextField textField_tel;
    private JTextField textField_indirizzo;
    private JTextField textField_eta;
    private JButton button_add;


    public Screen_add(Integer i){
        super("Editor");
        this.setContentPane(this.root_addPanel);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        if(i != -1){
            textField_cognome.setText(Screen_rubrica.persone.get(i).getCognome());
            textField_nome.setText(Screen_rubrica.persone.get(i).getNome());
            textField_tel.setText(Screen_rubrica.persone.get(i).getNum_telefono());
            textField_indirizzo.setText(Screen_rubrica.persone.get(i).getIndirizzo());
            textField_eta.setText(String.valueOf(Screen_rubrica.persone.get(i).getEta()));
        }
        button_add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if( i != -1){
                    Boolean check = validation();
                    if(check){
                        Persona p_canc = Screen_rubrica.persone.get(i);
                        Screen_rubrica.persone.remove(p_canc);
                        Persona p = new Persona(textField_nome.getText(),textField_cognome.getText(),textField_tel.getText(),textField_indirizzo.getText(),Integer.parseInt(textField_eta.getText()));
                        if( i == Screen_rubrica.persone.size() ){
                            Screen_rubrica.persone.add(p);
                        }
                        else{
                            Screen_rubrica.persone.add(i,p);
                        }
                        Screen_rubrica.refresh_file();
                        refresh_rubrica();
                        Screen_add.this.dispose();
                    }
                    else{
                        JOptionPane.showMessageDialog(null,"Alcuni campi non sono stati inseriti","Messaggio di Errore",JOptionPane.ERROR_MESSAGE);
                    }
                }
                else{
                    Boolean check = validation();
                    if(check){
                        Persona p = new Persona(textField_nome.getText(),textField_cognome.getText(),textField_tel.getText(),textField_indirizzo.getText(),Integer.parseInt(textField_eta.getText()));
                        Screen_rubrica.persone.add(p);
                        Screen_rubrica.refresh_file();
                        refresh_rubrica();
                        Screen_add.this.dispose();
                    }
                    else{
                        JOptionPane.showMessageDialog(null,"Alcuni campi non sono stati inseriti","Messaggio di Errore",JOptionPane.ERROR_MESSAGE);
                    }

                }
            }
        });
        button_annulla.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Screen_add.this.dispose();
            }
        });
    }

    private Boolean validation(){
        return !textField_indirizzo.getText().trim().isEmpty() && !textField_cognome.getText().trim().isEmpty() &&
                !textField_nome.getText().trim().isEmpty() && !textField_tel.getText().trim().isEmpty() &&
                !textField_eta.getText().trim().isEmpty();
    }

    public void refresh_rubrica(){
        int rowCount = Screen_rubrica.model.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            Screen_rubrica.model.removeRow(i);
        }
        for (Persona persona : Screen_rubrica.persone) {
            Object[] row = new Object[3];
            row[0] = persona.getNome();
            row[1] = persona.getCognome();
            row[2] = persona.getNum_telefono();
            Screen_rubrica.model.addRow(row);
        }
    }
}
